package main

import (
	"fmt"
	"os"
)

func main() {
	g := make(graph)

	i := 0
	for i == 0 {
		fmt.Println("===DETECT CYLE WITH READY GRAPH ===")
		fmt.Println("PLEASE CHOOSE -1- TO EXECUTE")

		fmt.Println("1. DETECT CYLE OR NOT CYCLE\n")

		fmt.Println("===DETECT CYLE MANUAL ENTER ===")
		fmt.Println("PLEASE ADD VERTEX'S AND EDGE'S AND CHOOSE -4- TO EXECUTE")

		fmt.Println("2. ADD A VERTEX")
		fmt.Println("3. ADD AN EDGE")
		fmt.Println("4. DETECT CYCLE WITH VALUES")
		fmt.Println("5. SIMPLE DISPLAY")
		fmt.Println("6. EXIT\n")

		var choice int
		fmt.Print("Enter your choice: ")
		fmt.Scanf("%d\n", &choice)
		switch choice {
		case 1:
			g.addVertexToGraph("0")
			g.addVertexToGraph("1")
			g.addVertexToGraph("2")
			g.addVertexToGraph("3")
			g.addVertexToGraph("4")
			g.addEdgeToGraph("1", "0")
			g.addEdgeToGraph("0", "2")
			g.addEdgeToGraph("2", "1")
			g.addEdgeToGraph("0", "3")
			g.addEdgeToGraph("3", "4")
			result, cycle := g.detectDFS()
			if cycle {
				fmt.Println("\n-- !!!GIVEN GRAPH CONTAIN CYCLE! --\n")
			} else if result != nil && !cycle {
				fmt.Println("\n-- !!!GIVEN DOESN'T CONTAIN CYCLE!!! --\n")
			}
			i := 0
			for i == 0 {

				fmt.Println("1. EXIT")
				fmt.Println("2. SIMPLE DISPLAY")

				var choice int
				fmt.Print("Enter your choice: ")
				fmt.Scanf("%d\n", &choice)
				switch choice {
				case 1:
					os.Exit(0)
				case 2:
					g.simpleDisplay()

				default:
					fmt.Println("Command not recognized.")
				}
			}

		case 2:
			g.addVertex()
		case 3:
			g.addEdge()
		case 4:

			result, cycle := g.detectDFS()
			if cycle {
				fmt.Println("\n-- !!!GIVEN GRAPH CONTAIN CYCLE! --\n")
			} else if result != nil && !cycle {
				fmt.Println("\n-- !!!GIVEN DOESN'T CONTAIN CYCLE!!! --\n")
			}
			i := 0
			for i == 0 {

				fmt.Println("1. EXIT")
				fmt.Println("2. SIMPLE DISPLAY")

				var choice int
				fmt.Print("Enter your choice: ")
				fmt.Scanf("%d\n", &choice)
				switch choice {
				case 1:
					os.Exit(0)
				case 2:
					g.simpleDisplay()

				default:
					fmt.Println("Command not recognized.")
				}
			}
		case 5:
			g.simpleDisplay()

		case 6:
			i = 1

		default:
			fmt.Println("Command not recognized.")
		}
	}
}

func (g graph) simpleDisplay() {
	fmt.Println("")
	for i := range g {
		fmt.Print(i, " => ")
		for j := range g[i] {
			fmt.Print(g[i][j] + " ")
		}
		fmt.Println("")
	}
}

// cyclic

// acyclic
// g.addVertexToGraph("0")
// g.addVertexToGraph("1")
// g.addVertexToGraph("2")
// g.addVertexToGraph("3")
// g.addVertexToGraph("4")
// g.addEdgeToGraph("0", "1")
// g.addEdgeToGraph("1", "2")
// g.addEdgeToGraph("2", "3")
// g.addEdgeToGraph("4", "2")

// cyclic

// acyclic
// g.addVertexToGraph("0")
// g.addVertexToGraph("1")
// g.addVertexToGraph("2")
// g.addVertexToGraph("3")
// g.addVertexToGraph("4")
// g.addEdgeToGraph("0", "1")
// g.addEdgeToGraph("0", "2")
// g.addEdgeToGraph("2", "3")
// g.addEdgeToGraph("2", "4")
