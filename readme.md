### Instructions
======Find a cycle in an undirected graph======

1. PLEASE INSTALL THE GO LANGUAGE ON YOU COMPUTER[golang download instructions](https://golang.org/doc/install).
2. AFTER INSTALLATION PLEASE GO THE DIRECTORY `cd cycle_algorithm`
3. Now `cd algorithms` into the folder where the files located
4. Now run ` go run . `

IF ANY ERROR WITH THIS ==>

"""go: go.mod file not found in current directory or any parent directory; see 'go help modules' """

1. PLEASE GO MAIN DIRECTORY `cd cycle_algorithm`  and apply `go mod init algorithms` 
2. AND ADD REQUIREMENTS `go mod tidy`
3. Now `cd algorithms` into the folder where the files located
4. Now run ` go run . `
EMIN MUGLA - UNIVERSITY OF FLORIDA